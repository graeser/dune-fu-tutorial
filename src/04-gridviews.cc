// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif



// included standard library headers
#include <iostream>

// included dune-common headers
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/stringutility.hh>

// included dune-grid headers
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

// included dune-fu-tutorial headers
#include "04-gridviews.hh"



template<class GridView>
void writeGridView(const GridView& gridView, std::string postFix)
{
  Dune::VTKWriter<GridView> vtkWriter(gridView);
  vtkWriter.write(std::string("04-gridviews-")+postFix);
}

template<class Grid>
void writeAllGridViews(const Grid& grid, std::string gridName)
{
  for(int level = 0; level <= grid.maxLevel(); ++level)
    writeGridView(grid.levelGridView(level), Dune::formatString(gridName+"-level-%02d", level));
  
  writeGridView(grid.leafGridView(), gridName+"-leaf");
}



int main(int argc, char** argv)
{
  try{
    // Maybe initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    // Print process rank
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      std::cout<<"I am rank "<<helper.rank()<<" of "<<helper.size()
        <<" processes!"<<std::endl;

    {
      using Grid = Dune::YaspGrid<2>;
      auto gridPointer = createCubeGrid<Grid>();
      Grid& grid = *gridPointer;
      grid.globalRefine(2);
      writeAllGridViews(grid, "2d_yaspgrid");
    }

    {
      using Grid = Dune::YaspGrid<3>;
      auto gridPointer = createCubeGrid<Grid>();
      Grid& grid = *gridPointer;
      grid.globalRefine(2);
      writeAllGridViews(grid, "3d_yaspgrid");
    }

    {
      using Grid = Dune::UGGrid<2>;
      auto gridPointer = createSimplexGrid<Grid>();
      Grid& grid = *gridPointer;
      grid.globalRefine(2);
      
      for(int i=0; i<2; ++i)
      {
        auto gridView = grid.levelGridView(grid.maxLevel());
        auto it = Dune::elements(gridView).begin();
        grid.mark(1, *it);
        grid.adapt();
      }

      writeAllGridViews(grid, "2d_uggrid");
    }

    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
