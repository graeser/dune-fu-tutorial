// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif



// included standard library headers
#include <iostream>
#include <array>

// included dune-common headers
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

// included dune-grid headers
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>



int main(int argc, char** argv)
{
  try{
    // Maybe initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    // Print process rank
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      std::cout<<"I am rank "<<helper.rank()<<" of "<<helper.size()
        <<" processes!"<<std::endl;

    // fix grid dimension
    const int dim = 2;
    
    // use the YaspGrid implementation
    // YaspGrid = "Yet another structured parallel grid"
    using Grid = Dune::YaspGrid<dim>;

    // extension of the domain [0,l]
    Dune::FieldVector<double,dim> l(1);

    // start with 10x10x... elements
    std::array<int,dim> E;
    for(auto& e : E)
      e = 2;

    Grid grid(l, E);

    grid.globalRefine(2);

    auto gridView = grid.leafGridView();

    int i = 0;
    for(const auto& e: Dune::elements(gridView))
    {
      ++i;
      std::cout << e.geometry().center() << std::endl;
    }
    std::cout << i << std::endl;


    using GridView = decltype(gridView);
    Dune::VTKWriter<GridView> vtkWriter(gridView);
    vtkWriter.write("02-basic-grid-interface");

    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
