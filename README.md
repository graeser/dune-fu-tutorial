# Getting and building this tutorial

This document gives a quick introduction in how to
get and build Dune. Be warned that this document is work in progress.
If you find an error feel free to open a corresponding issue.
You may also want to have a look at the excellent
[getting started with Dune](http://www.math.tu-dresden.de/~osander/research/sander-getting-started-with-dune.pdf)
text by written by Oliver Sander.

## Prerequisites

### Operation system

In order to use Dune you will need a recent Linux installation.
People are also using Dune on OSX and it has been reported you
can build Dune on Windows after applying some patches. Both systems
will not be covered here. However, the following instructions
may (or may not) in large parts also apply to OSX.


### Required tools

In order to obtain Dune via the `git` version control system you must install

* git

on your system.
Dune is written in modern C++, so you will need a C++-compiler which
is not to outdated. This does in general mean that you need either
of the following compilers:

* gcc (version 4.9 or newer)
* clang (version 3.? or newer)

Furthermore Dune uses the CMake build-system and requires

* cmake (version 2.8.12 or newer)

Any recent Linux distribution will contain these tools in its
repository. Hence you should be able to install them using
the package management system of your distribution. It is not
recommended to install these tools manually from source or from
binary packages unless you are an advanced user and know what
you're doing.




## Getting Dune

There are several ways to get Dune. One option is to install it
via your distributions package management system if it provides
Dune. This is e.g. the case for Debian and Ubuntu. Another option
that we will follow here is to build them from source after getting
the code directly from the central source code repository.
First we create a directory for our Dune installation e.g. by

```shell
mkdir dune
cd dune
```

Next we clone the repositories for the core modules

```shell
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
```

Besides the core modules we need some extension modules

```shell
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
git clone https://gitlab.dune-project.org/staging/dune-functions.git
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
```

Finally this tutorial module

```shell
git clone git@git.imp.fu-berlin.de:graeser/dune-fu-tutorial.git
```


## Configuring and building Dune

Dune uses a CMake build system. Each Dune module is build individually but
most modules will depend on others being build before. In order to avoid
having to do this manually Dune provides the script `dunecontrol` which
resolves the module dependencies, builds them in an appropriate order,
and passes options in a unified way. You can pass options to this script
via an options file. In the following we assume that the absolute
path of our Dune directory is `/home/john/dune`. Then you can e.g. create
a file `/home/john/dune/debug.opts` with the following content

```
# Setup a central build directory for all modules
BUILDDIR="/home/john/dune/build-debug"

# Point cmake to dependencies
CMAKE_PREFIX_PATH=""

# Used compiler
# for gcc use g++, for clang use clang++
CXX=/usr/bin/g++

# Use CMAKE_PREFIX_PATH to tell CMake some specific stuff, e.g. to use a Debug build
CMAKE_FLAGS="
    -DCMAKE_CXX_COMPILER='$CXX' \
    -DCMAKE_CXX_FLAGS='-g -O0 -Wall -Wno-sign-compare' \
    -DCMAKE_PREFIX_PATH='$CMAKE_PREFIX_PATH' \
"
```

This contains a set of options suitable for a debug build.
Now you can build Dune with the given options file by calling

```shell
./dune-common/bin/dunecontrol --opts=debug.opts all
```

This will generate a directory `build-debug` which contains
a build folder for any Dune module. The compiled example programs
of this tutorial are contained in `build-debug/dune-fu-tutorial/src/`.

If you want to recompile after changing code you will normally not need
to run `dunecontrol` again. Instead you can simply call `make` in the
**build-directory** for the corresponding module.


